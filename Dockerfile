# base image
FROM python:3.11.1

# set the workdir into /app
WORKDIR /usr/src/app

# copy the requeriments.txt file into the container
COPY requirements.txt /usr/src/app/

# install all the dependencies
RUN python -m pip install --no-cache-dir -r requirements.txt

# copy all the files of the app into the container
COPY . /usr/src/app/