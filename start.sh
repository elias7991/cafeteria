#!/bin/sh

# create de requirements for run Dockerfile
python -m pip freeze > requirements.txt

# run the compose
docker-compose up -d